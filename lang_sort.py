# check for the top ten language used in tweets harvested

import json
import couchdb
import requests
from pprint import pprint

view = 'http://localhost:5984/test/_design/lang_view/_view/language-view'
payload = {'reduce':'true','group':'true'}
results = requests.get(view, payload)
view_json = json.loads(results.text)

attribute_count = {}
for row in view_json['rows']:
    attribute_count[row['key'].encode('utf-8')] = row['value']

sorted_attributes = sorted(attribute_count.items(), key=lambda x: x[1], reverse=True)[:10]
pprint(sorted_attributes)
