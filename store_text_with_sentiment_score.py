import sys
import json, geojson
import couchdb
from my_couchdb import couchdb_pager

'''
a once-off script which adds the tweet's text into the sentiment score database
to allow the ability to map-reduce based on keyword in sentiment_scored database
'''

SENT_DB_NAME = 'sentiment_scored'
USERNAME = 'azhou'
PASSWORD = 'potato'
TWT_DB_NAME = 'melbourne'

# Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
twt_db = server[TWT_DB_NAME]
sent_db = server[SENT_DB_NAME]

print('Connected to ' + server_url)

# Check through all tweets in sentiment databse and add the text into the doc
for k in couchdb_pager(sent_db):
    #ignore views
    if k[:1] == '_':
        continue

    # the tweet id must be in the tweet database, no guard check
    doc = sent_db.get(k)
    doc['text'] = twt_db[k]['text']
    sent_db.save(doc)
    print(doc)
