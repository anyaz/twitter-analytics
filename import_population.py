
import couchdb
import json

DB_NAME = 'population'

USERNAME = 'azhou'
PASSWORD = 'potato'

server = couchdb.Server()
server.resource.credentials = (USERNAME, PASSWORD)

try:
    db = server[DB_NAME]
except:
    db = server.create(DB_NAME)

aurin_file = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/aurin/population/data8543771302952440319.json'

# import the area code and unemployment rate for each sa2 area
with open(aurin_file) as json_data:
    regions = json.load(json_data)['features']
    for region in regions:
        region_data = {}
        region_data['population'] = region['properties']['ERP_2015']
        # print(region_data)
        # print(region['properties']['area_code'])
        db[str(region['properties']['SA2_MAIN11'])] = region_data
