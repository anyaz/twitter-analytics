# check for the top ten language used in tweets harvested

import json
import couchdb
import requests
from pprint import pprint
from collections import defaultdict, Counter

def coords_from_view():
    view = 'http://115.146.87.112:5984/processed_tweet_db/_design/for_visualization/_view/new-view'
    payload = {'limit':'10','reduce':'false'}
    results = requests.get(view, payload)
    view_json = json.loads(results.text)

    print(view_json)

    # coords = []
    # for row in view_json['rows']:
    #     print(str(row['key']['coordinates']))

    #     coords.append(row['key']['coordinates'])
    # return coords

coords_from_view()
