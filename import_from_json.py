# imports a property field from an AURIN JSON file into a couchdb database
import sys
import couchdb
import json
from my_couchdb import set_up_db

# # unemployment
# JSON_FILE = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/aurin/unemployment_rate/data7526447955839468298.json'
# DB_NAME = 'unemployment_rate'
# CODE_COL_NAME = 'sa2_main11'
# DATA_COL_NAME = 'lf_4'

# income support
JSON_FILE = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/aurin/income_support/data7775971370364962464.json'
DB_NAME = 'income_support'
CODE_COL_NAME = 'area_code'
DATA_COL_NAMES = ['unemply_ben_3_percent_6_13_6_13','unem_bn_yng_3_percent_6_13_6_13']
DATA_COL_RENAMES = ['unem_ben_perc', 'unem_ben_perc_16_to_24']

USERNAME = 'azhou'
PASSWORD = 'potato'

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)

db = set_up_db(DB_NAME, server)

# import the area code and data for each sa2 area
with open(JSON_FILE) as json_data:
    regions = json.load(json_data)['features']
    for region in regions:
        region_data = {}
        for i, val in enumerate(DATA_COL_NAMES):
            region_data[DATA_COL_RENAMES[i]] = region['properties'][val]

        # progress print out
        print('{}: {}'.format(region['properties'][CODE_COL_NAME], region_data))

        db[str(region['properties'][CODE_COL_NAME])] = region_data
