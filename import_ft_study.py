
import couchdb
import json

DB_NAME = 'fulltime_study'

USERNAME = 'azhou'
PASSWORD = 'potato'

server = couchdb.Server()
server.resource.credentials = (USERNAME, PASSWORD)

db = server[DB_NAME]

aurin_file = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/aurin/full_time_study_perc/data6604799219385357176.json'

# import the area code and unemployment rate for each sa2 area
with open(aurin_file) as json_data:
    regions = json.load(json_data)['features']
    for region in regions:
        region_data = {}
        region_data['ft_study_perc'] = region['properties']['youth_6']
        # print(region_data)
        db[region['properties']['sa2_main11']] = region_data
