# check for the top ten language used in tweets harvested

import json
import couchdb
import requests
from pprint import pprint
from collections import defaultdict, Counter
from geojson import MultiPolygon


USERNAME = 'azhou'
PASSWORD = 'potato'

server = couchdb.Server()
server.resource.credentials = (USERNAME, PASSWORD)

db = server['test']
aurin_db = server['lang']

# count the number of tweets by region and language
tweet_area_lang = defaultdict(Counter)
for row in db.view('_design/place_view/_view/vic_view'):
    tweet_area_lang[row.value['place']['full_name']][row.value['lang']] += 1

# extract the language spoken person stats from AURIN dataset
aurin_area_lang = defaultdict(dict)
for row in aurin_db:
    aurin_area_lang[aurin_db[row]['REGION']][aurin_db[row]['LANP']] = aurin_db[row]['MEASURE_3']




# pprint(area_lang)

# attribute_count = {}
# for row in view_json['rows']:
#     attribute_count[row['key'].encode('utf-8')] = row['value']
#
# sorted_attributes = sorted(attribute_count.items(), key=lambda x: x[1], reverse=True)[:10]
# pprint(sorted_attributes)
