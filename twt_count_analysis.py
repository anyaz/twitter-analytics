import couchdb
import shapefile
import json
import geojson
from shapely.geometry import shape, Point, Polygon, MultiPolygon
from collections import Counter
import matplotlib.pyplot as plt

GEO_DB_NAME = 'geo'
COUNT_DB_NAME = 'tweet_count'
AURIN_DB_NAME = 'population'
# USERNAME = 'azhou'
# PASSWORD = 'potato'

PLOT_FILE = 'population.png'

server = couchdb.Server('http://115.146.91.219:5984')
# server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
c_db = server[COUNT_DB_NAME]
aurin_db = server[AURIN_DB_NAME]

counts = []
aurin_stats = []
region_names = []

for r in c_db:
    counts.append(c_db[r]['count'])
    aurin_stats.append(aurin_db[r]['population'])
    region_names.append(geo_db[r]['name'])

print(counts)
print(aurin_stats)
print(region_names)

plt.scatter(aurin_stats, counts, alpha=0.5)
plt.ylabel('Number of tweets')
plt.xlabel('Population')
# x_pos = np.arange(top_num)
#
# plt.bar(x_pos, top_counts, align='center', alpha=0.5)
# plt.xticks(x_pos, top_names, rotation=45)
#
plt.savefig(PLOT_FILE)
