import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

USERNAME = 'azhou'
PASSWORD = 'potato'
GEO_DB_NAME = 'geo'
SENT_DB_NAME = 'sentiment_scored'

# AURIN_DB_NAME = 'income'
## mean income correlation
# PLOT_FILE = 'mean_income_vs_sentiment.png'
# X_LABEL = 'Mean Income ($)'
# AURIN_COL_NAME = 'Mean'

## median income correlation
# PLOT_FILE = 'median_income_vs_sentiment.png'
# X_LABEL = 'Median Income ($)'
# AURIN_COL_NAME = 'Median'

# AURIN_DB_NAME = 'unemployment_rate'
# # unemployment rate correlation
# PLOT_FILE = 'unemployment_rate_vs_sentiment.png'
# X_LABEL = 'Unemployement Rate %'
# AURIN_COL_NAME = AURIN_DB_NAME

AURIN_DB_NAME = 'income_support'
# unemployment rate correlation
PLOT_FILE = 'young_people_income_support_perc_vs_sentiment.png'
X_LABEL = 'Young People Aged 16 to 24 Receiving an Unemployment Benefit %'
AURIN_COL_NAME = 'unem_ben_perc_16_to_24'

Y_LABEL = 'Average Tweet Sentiment Score'
MIN_SCORES_REQ = 10

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# DB_NAME = input('Enter the name of the geo-area database: ')
# DB_NAME = input('Enter the name of the new database: ')
# FILE = input('Enter the path to the import file: ')
# CODE_COL_NAME = input('Enter the name of the column with the region code: ')

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
aurin_db = server[AURIN_DB_NAME]
sent_db = server[SENT_DB_NAME]
sent_sum_view = sent_db.view('_design/score-view/_view/sum-score-view', reduce=True, group=True)
sent_count_view = sent_db.view('_design/score-view/_view/count-score-view', reduce=True, group=True)


avg_sent_scores = []
aurin_stats = []
region_names = []

for row in sent_sum_view:
    # can only correlate it if we have the aurin stats for that region
    if row.key in aurin_db and aurin_db[row.key][AURIN_COL_NAME] is not None:
        sent_sum = row.value
        # get the corresponding row count
        sent_count = list(sent_count_view[row.key])[0].value

        # only include those regions with >10 sentiments recorded (reduce bias)
        if sent_count > MIN_SCORES_REQ:
            avg_sent_score = sent_sum / sent_count

            # populate the three lists in order
            avg_sent_scores.append(avg_sent_score)
            aurin_stats.append(aurin_db[row.key][AURIN_COL_NAME])
            region_names.append(geo_db[row.key]['name'])


plt.scatter(aurin_stats, avg_sent_scores, alpha=0.5)
aurin_stats = np.array(aurin_stats).reshape(-1,1)
avg_sent_scores = np.array(avg_sent_scores).reshape(-1,1)

# Create linear regression
regr = linear_model.LinearRegression()
regr.fit(aurin_stats, avg_sent_scores)

# Regression coefficients
print('Coefficients: \n', regr.coef_)
print("Mean squared error: %.2f"
      % np.mean((regr.predict(aurin_stats) - avg_sent_scores) ** 2))
print('Variance score: %.2f' % regr.score(aurin_stats, avg_sent_scores))
plt.plot(aurin_stats, regr.predict(aurin_stats), color='red', linewidth=1)

plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)
plt.savefig(PLOT_FILE)

# # note that some regions are missing income information...
# for k in geo_db:
#     if k not in aurin_db:
#         print(k+','+geo_db[k]['name'])
