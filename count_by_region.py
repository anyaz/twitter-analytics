import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter

USERNAME = 'azhou'
PASSWORD = 'potato'
GEO_DB_NAME = 'geo'
SENT_DB_NAME = 'sentiment_scored'

TOP_NUM = 5
PLOT_FILE = 'top_5_region_tweets_count.png'
Y_LABEL = 'Number of tweets'
X_LABEL = 'Region Name'

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# DB_NAME = input('Enter the name of the geo-area database: ')
# DB_NAME = input('Enter the name of the new database: ')
# FILE = input('Enter the path to the import file: ')
# CODE_COL_NAME = input('Enter the name of the column with the region code: ')

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
sent_db = server[SENT_DB_NAME]
sent_count_view = sent_db.view('_design/score-view/_view/count-score-view', reduce=True, group=True)

areas_counter = Counter()

for row in sent_count_view:
    areas_counter[row.key] = row.value

#get the top 5 regions as a list of tuples
top_regions = areas_counter.most_common(TOP_NUM)
top_names = []
top_counts = []

for (k, v) in top_regions:
    top_names.append(geo_db[k]['name'])
    top_counts.append(v)

x_pos = np.arange(TOP_NUM)

plt.bar(x_pos, top_counts, align='center', alpha=0.5)
plt.xticks(x_pos, top_names, rotation=45)
plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)

plt.savefig(PLOT_FILE, bbox_inches = 'tight')
