import sys
import json, geojson
from shapely.geometry import shape, Point, Polygon, MultiPolygon
import couchdb
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

'''
daemon which process and map all english tweets' sentiment scores to
specific geographic regions
'''

SENT_DB_NAME = 'sentiment_scored'
OOB_DB_NAME = 'out_of_bound'
INVALID = 'invalid'

def set_up_db(db_name, server):
    try:
        return server[db_name]
    except:
        return server.create(db_name)

def add_to_areas_dict(areas):
    # add the MultiPolygon from each area
    for key in geo_db:
        # ignore views
        if key[:1] == '_':
            continue

        if geo_db[key]['geometry'] is not None:
            area_str = json.dumps(geo_db[key]['geometry'])
            areas[key] = shape(geojson.loads(area_str))

def convert_to_point(coordinates):
    point_str = json.dumps(coordinates)
    coords = geojson.loads(point_str)
    return shape(coords)

def get_region_code(point, areas):
    for key, area_shape in areas.items():
        try:
            if area_shape.contains(point):
                return key #assumes no overlapping regions, only count in first one
        except: #in case that something's wrong with the shape
            pass
    return INVALID

def get_sentiment_scores(sentence):
    analyser = SentimentIntensityAnalyzer()
    snt = analyser.polarity_scores(sentence)
    return snt

# function for dealing with couchdb pagination on large dbs, code sourced from:
# https://lambdafu.net/2011/09/17/a-better-iterator-for-python-couchdb/
def couchdb_pager(db, view_name='_all_docs', query=None,
                  startkey=None, startkey_docid=None,
                  endkey=None, endkey_docid=None, bulk=5000):

    # Request one extra row to resume the listing there later.
    options = {'limit': bulk + 1}
    if startkey:
        options['startkey'] = startkey
        if startkey_docid:
            options['startkey_docid'] = startkey_docid
    if endkey:
        options['endkey'] = endkey
        if endkey_docid:
            options['endkey_docid'] = endkey_docid
    done = False
    while not done:
        # query gets to override any mention of view
        if query is not None:
            view = db.query(query, **options)
        else:
            view = db.view(view_name, **options)
        rows = []
        # If we got a short result (< limit + 1), we know we are done.
        if len(view) <= bulk:
            done = True
            rows = view.rows
        else:
            # Otherwise, continue at the new start position.
            rows = view.rows[:-1]
            last = view.rows[-1]
            options['startkey'] = last.key
            options['startkey_docid'] = last.id

        for row in rows:
            yield row.id

USERNAME = 'azhou'
PASSWORD = 'potato'
TWT_DB_NAME = 'melbourne'
# TWT_DB_NAME = 'test'
GEO_DB_NAME = 'geo'

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# TWT_DB_NAME = input('Enter the name of the tweet database: ')
# GEO_DB_NAME = input('Enter the name of the geo-area database: ')

# Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
twt_db = server[TWT_DB_NAME]
geo_db = server[GEO_DB_NAME]

# Need a databases to store the sentiment scores & summary for each geo region
sent_db = set_up_db(SENT_DB_NAME, server)
oob_db = set_up_db(OOB_DB_NAME, server)
print('Connected to ' + server_url)

areas = {}
add_to_areas_dict(areas)
print('Area shapes loaded from geo-area database')

# Continues loop to process any new tweets coming in from the harvester
while True:
    print('Checking all tweets harvested...')
    for k in couchdb_pager(twt_db):
        #ignore views
        if k[:1] == '_':
            continue

        #skip if already processed before
        if k in sent_db or k in oob_db:
            continue

        doc = twt_db[k]
        # only process english tweets (translation not implemtented
        # due to cap placed on google's translation service)
        if doc['lang'] != 'en' or (doc['coordinates'] is None):
            continue

        print('Processing newly harvested english tweet: ' + k)
        # get the point shape from the coordinates
        point = convert_to_point(doc['coordinates'])

        # proceed if tweet can be mapped to a valid aurin region
        region = get_region_code(point, areas)

        if region != INVALID:
            # get scores, update geo summary & store in sentiment db
            sent_scores = get_sentiment_scores(doc['text'])
            region_field = {}
            region_field['geo_code'] = region
            sent_scores.update(region_field)
            sent_scores['text'] = doc['text']
            sent_db[k] = sent_scores
        else:
            # record it to avoid processing it again...
            oob_db[k] = doc['coordinates']
    # break; # uncomment to run once-off scoring
