import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter

USERNAME = 'azhou'
PASSWORD = 'potato'
DB_NAME = 'melbourne'

TOP_NUM = 5

# # user view
# KEY_NAME = 'user'
# PLOT_FILE = 'top_5_user_tweets_count.png'
# Y_LABEL = 'Number of tweets'
# X_LABEL = 'Username'

# # hashtag view
# KEY_NAME = 'tag'
# PLOT_FILE = 'top_10_hashtags_featured_in_tweets_count.png'
# Y_LABEL = 'Number of tweets'
# X_LABEL = 'Hashtag'

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
db = server[DB_NAME]
count_view = db.view('_design/' + KEY_NAME + '-view/_view/count-' + KEY_NAME + '-view', reduce=True, group=True)

counter = Counter()

for row in count_view:
    counter[row.key] = row.value

#get the top 5 keys as a list of tuples
top_keys = counter.most_common(TOP_NUM)
top_names = []
top_counts = []

for (k, v) in top_keys:
    top_names.append('#'+k)
    top_counts.append(v)

x_pos = np.arange(TOP_NUM)

plt.bar(x_pos, top_counts, align='center', alpha=0.5)
plt.xticks(x_pos, top_names, rotation=45)
plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)

plt.savefig(PLOT_FILE, bbox_inches = 'tight')
