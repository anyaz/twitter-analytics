# imports sa2 location data into couchbd

import json
import couchdb
import pandas

USERNAME = 'azhou'
PASSWORD = 'potato'

server = couchdb.Server()
server.resource.credentials = (USERNAME, PASSWORD)

db = server['sa2']

file = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/location/SA2_2016_AUST.csv'

sa2_df = pandas.read_csv(file)

sa2_json_str = sa2_df.filter(items = ['SA2_MAINCODE_2016', 'SA2_NAME_2016']).to_json(orient='records')

sa2_json = json.loads(sa2_json_str)

for sa2 in sa2_json:
    doc = {'_id': str(sa2['SA2_MAINCODE_2016'])}
    doc.update(sa2)
    print doc
    db.save(doc)
