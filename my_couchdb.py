import couchdb

def set_up_db(db_name, server):
    try:
        return server[db_name]
    except:
        return server.create(db_name)

# function for dealing with couchdb pagination on large dbs, code sourced from:
# https://lambdafu.net/2011/09/17/a-better-iterator-for-python-couchdb/
def couchdb_pager(db, view_name='_all_docs', query=None,
                  startkey=None, startkey_docid=None,
                  endkey=None, endkey_docid=None, bulk=5000):

    # Request one extra row to resume the listing there later.
    options = {'limit': bulk + 1}
    if startkey:
        options['startkey'] = startkey
        if startkey_docid:
            options['startkey_docid'] = startkey_docid
    if endkey:
        options['endkey'] = endkey
        if endkey_docid:
            options['endkey_docid'] = endkey_docid
    done = False
    while not done:
        # query gets to override any mention of view
        if query is not None:
            view = db.query(query, **options)
        else:
            view = db.view(view_name, **options)
        rows = []
        # If we got a short result (< limit + 1), we know we are done.
        if len(view) <= bulk:
            done = True
            rows = view.rows
        else:
            # Otherwise, continue at the new start position.
            rows = view.rows[:-1]
            last = view.rows[-1]
            options['startkey'] = last.key
            options['startkey_docid'] = last.id

        for row in rows:
            yield row.id
