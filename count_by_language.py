import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter

USERNAME = 'azhou'
PASSWORD = 'potato'
LABEL_DB_NAME = 'twt_lang'
db_NAME = 'melbourne'

TOP_NUM = 5
# language view
KEY_NAME = 'lang'
PLOT_FILE = 'top_5_lang_tweets_count.png'
Y_LABEL = 'Number of tweets'
X_LABEL = 'Foreign Language'

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# DB_NAME = input('Enter the name of the geo-area database: ')
# DB_NAME = input('Enter the name of the new database: ')
# FILE = input('Enter the path to the import file: ')
# CODE_COL_NAME = input('Enter the name of the column with the region code: ')

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
lang_db = server[LABEL_DB_NAME]
db = server[db_NAME]
count_view = db.view('_design/' + KEY_NAME + '-view/_view/count-' + KEY_NAME + '-view', reduce=True, group=True)

areas_counter = Counter()

for row in count_view:
    areas_counter[row.key] = row.value

#get the top 5 regions as a list of tuples
top_regions = areas_counter.most_common(TOP_NUM)
top_names = []
top_counts = []


for (k, v) in top_regions:
    try:
        top_names.append(lang_db[k]['Name'])
    except:
        print(k)
    top_counts.append(v)


x_pos = np.arange(TOP_NUM)

plt.bar(x_pos, top_counts, align='center', alpha=0.5)
plt.xticks(x_pos, top_names, rotation=45)
plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)

plt.savefig(PLOT_FILE, bbox_inches = 'tight')
