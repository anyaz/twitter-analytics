import couchdb
import shapefile
import json
import geojson
from shapely.geometry import shape, Point, Polygon, MultiPolygon
from collections import Counter

GEO_DB_NAME = 'geo'
# TWEET_DB_NAME = 'test'
TWEET_DB_NAME = 'melbourne'
COUNT_DB_NAME = "tweet_count"
# USERNAME = 'azhou'
# PASSWORD = 'potato'

server = couchdb.Server()
# server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
twt_db = server[TWEET_DB_NAME]
c_db = server[COUNT_DB_NAME]

def add_to_areas_dict(areas):
    # add the MultiPolygon from each area
    for key in geo_db:
        area_str = json.dumps(geo_db[key]['geometry'])
        areas[key] = shape(geojson.loads(area_str))

def update_area_count(point, areas, areas_counter):
    for a_id, a_area in areas.items():
        if a_area.contains(point):
            areas_counter[a_id] += 1
            break; #done, assumes no overlapping areas
            # print(point)

##### loop through all tweets in db and performs count for all regions #####

areas = {}
areas_counter = Counter()

#initialize the areas data from couchdb
add_to_areas_dict(areas)

## tester point for debug
# point = Point([144.95, -37.75])
# update_area_count(point, areas, areas_counter)

#process the tweets and tally the area counts for those with coords
for key in twt_db:
    try:
        #pull the coords out, if they exist
        point_str = json.dumps(twt_db[key]['coordinates'])
        coords = geojson.loads(point_str)
        if coords is not None:
            #make it a point shape
            point = shape(coords)

            #update the area counter with the point
            update_area_count(point, areas, areas_counter)
    except:
        #no coords field in tweet
        pass

#save the count into the count db
for k, v in areas_counter.items():
    count = {}
    count['count'] = v
    c_db[k] = count
