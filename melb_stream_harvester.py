import tweepy
import couchdb
import json
import sys
import tweepy
from my_couchdb import set_up_db

'''
Harvests tweets within the Greater Melbourne area and stores the harvested
tweets into a couchdb database
'''

USERNAME = 'azhou'
PASSWORD = 'potato'
TWT_DB_NAME = 'melbourne'

# the bounding box for the Greater Melbourne area (per AURIN selection box)
FILTER_AREA_BBOX = [144.3336,-38.5030,145.8784,-37.1751]

# Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
db = set_up_db(TWT_DB_NAME, server)

# Twitter API authentication details.
consumer_key = 'ZU1WzM5NWmzTXjqQPA61kiJqP'
consumer_secret = 'mojkdQvzosP3cEOCvRndamjHtAdBe0V1qyrCAnplFcSz6jqbnD'
access_token = '117961021-xsMPvxrDndSNXcvcE34ilavlJ0eI3ZdZyepfhMXZ'
access_token_secret = 'Bk9UH7o4ZrBdLyxjI17JMsWw40xAhM0stqo2x9XZROi8b'

class MyStreamListener(tweepy.StreamListener):
    def on_status(self, status):
        try:
            doc=status._json
            doc['_id']=doc['id_str']
            del doc['id_str']
            db.save(doc)
            print('New tweet! ID: %s' % doc['_id'])

        except couchdb.http.ResourceConflict:
            # duplicates in stream expected when running multiple harvesters
            pass # keep going

        except tweepy.RateLimitError:
            print("Rate limited!")
            time.sleep(15*60) # 15 minutes sleep

        except Exception as e:
            print('Error: %s' % e)

    def on_error(self, status_code):
        print('Error: %s' % status_code)

while True:
    try:
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        twitterStream = tweepy.Stream(auth, MyStreamListener())
        twitterStream.filter(locations=FILTER_AREA_BBOX)

    except Exception as e:
        print('Error: %s' % e)
