import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter

USERNAME = 'azhou'
PASSWORD = 'potato'
GEO_DB_NAME = 'geo'
SENT_DB_NAME = 'sentiment_scored'

TOP_NUM = 5
PLOT_FILE = 'bottom_5_region_average_sentiment.png'
Y_LABEL = 'Average Sentiment Score'
X_LABEL = 'Region Name'
MIN_SCORES_REQ = 10

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# DB_NAME = input('Enter the name of the geo-area database: ')
# DB_NAME = input('Enter the name of the new database: ')
# FILE = input('Enter the path to the import file: ')
# CODE_COL_NAME = input('Enter the name of the column with the region code: ')

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
sent_db = server[SENT_DB_NAME]

sent_sum_view = sent_db.view('_design/score-view/_view/sum-score-view', reduce=True, group=True)
sent_count_view = sent_db.view('_design/score-view/_view/count-score-view', reduce=True, group=True)

avg_sent_scores = {}
top_scores = []
top_names = []

for row in sent_sum_view:
    sent_sum = row.value
    # get the corresponding row count
    sent_count = list(sent_count_view[row.key])[0].value

    # only include those regions with >10 sentiments recorded (reduce bias)
    if sent_count > MIN_SCORES_REQ:
        avg_sent_scores[row.key] = sent_sum / sent_count

top_regions = sorted(avg_sent_scores, key=avg_sent_scores.get, reverse=False)[:TOP_NUM]

for r in top_regions:
    top_scores.append(avg_sent_scores[r])
    top_names.append(geo_db[r]['name'])

x_pos = np.arange(TOP_NUM)

plt.bar(x_pos, top_scores, align='center', alpha=0.5)
plt.xticks(x_pos, top_names, rotation=45)
plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)

plt.savefig(PLOT_FILE, bbox_inches = 'tight')
