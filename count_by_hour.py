import sys
import couchdb
import matplotlib.pyplot as plt
import numpy as np
from collections import OrderedDict

USERNAME = 'azhou'
PASSWORD = 'potato'
DB_NAME = 'melbourne'

# hour view
KEY_NAME = 'time'
PLOT_FILE = 'alt_hourly_tweets_count.png'
Y_LABEL = 'Number of tweets'
X_LABEL = 'Hour'

def hour_lable(utc_h):
    # UTC to AEST conversion
    h = (utc_h + 10) % 24
    if h > 11:
        if h == 12:
            return str(h) + 'pm'
        return str(h-12) + 'pm'
    else:
        if h == 0:
            return '12am'
        return str(h)+'am'

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
db = server[DB_NAME]
count_view = db.view('_design/' + KEY_NAME + '-view/_view/count-' + KEY_NAME + '-view', reduce=True, group=True)

counter = {}

# view already sorted by ascending hours, so already in chronological order
for row in count_view:
    counter[row.key] = row.value

names = []
counts = []

for k, v in counter.items():
    names.append(hour_lable(k))
    # names.append(k)
    counts.append(v)

x_pos = np.arange(len(counter))

plt.figure(figsize=(15,5))
plt.bar(x_pos, counts, align='center', alpha=0.5)
plt.xticks(x_pos, names, rotation=45)
plt.ylabel(Y_LABEL)
plt.xlabel(X_LABEL)

plt.savefig(PLOT_FILE, bbox_inches = 'tight')
