# imports all retional stats from a csv file into a couchdb database
import sys
import json
import couchdb
import pandas
from my_couchdb import set_up_db

USERNAME = 'azhou'
PASSWORD = 'potato'
DB_NAME = 'twt_lang'
FILE = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/lang/lang_name.csv'
CODE_COL_NAME = 'Language_code'

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
db = set_up_db(DB_NAME, server)

data_df = pandas.read_csv(FILE)
data_json_str = data_df.to_json(orient='records')
data_json = json.loads(data_json_str)

for row in data_json:
    doc = {'_id': str(row[CODE_COL_NAME])}
    del row[CODE_COL_NAME]
    doc.update(row)
    db.save(doc)
    print(doc)
