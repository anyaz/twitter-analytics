import tweepy
import json
import couchdb

USERNAME = 'azhou'
PASSWORD = 'potato'

# Twitter API authentication details.
consumer_key = 'ZU1WzM5NWmzTXjqQPA61kiJqP'
consumer_secret = 'mojkdQvzosP3cEOCvRndamjHtAdBe0V1qyrCAnplFcSz6jqbnD'
access_token = '117961021-xsMPvxrDndSNXcvcE34ilavlJ0eI3ZdZyepfhMXZ'
access_token_secret = 'Bk9UH7o4ZrBdLyxjI17JMsWw40xAhM0stqo2x9XZROi8b'

# CouchDB authentication details.
# tweetdb = couchdb.Server()['test']
# userdb = couchdb.Server()['user']

couch = couchdb.Server()
couch.resource.credentials = (USERNAME, PASSWORD)

tweetdb = couch['test']
userdb = couch['user']
historydb = couch()['history']

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

def rate_limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            print("capped")
            time.sleep(15*60)

while True:
    for id in tweetdb:
        try:
            username = tweetdb[id]['user']['screen_name']
            user_searched = False

            for user in userdb:
                if user == username:
                    user_searched = True
                    break

            if not user_searched:
                # grab all tweets from the user's history
                for status in rate_limit_handled(tweepy.Cursor(api.user_timeline, id = username).items(10)):
                    doc = {'_id': status._json['id_str']}
                    doc.update(status._json)
                    print(doc)
                    historydb.save(doc)

                # keep track of searched users
                user = {'_id':username}
                userdb.save(user)

        except couchdb.http.ResourceConflict:
            # duplicates in search & stream is expected, move on
            pass

        except Exception as e:
            print e
