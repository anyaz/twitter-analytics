import shapefile
from shapely import geometry

DB_NAME = 'geo'

USERNAME = 'azhou'
PASSWORD = 'potato'

server = couchdb.Server()
server.resource.credentials = (USERNAME, PASSWORD)

db = server[DB_NAME]

shp_file = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/geo/data2864328704832097389.json'

# import the area name & area geometry for each sa2 area in the aurin spatial mapping json
with open(shp_file) as json_data:
    regions = json.load(json_data)['features']
    for region in regions:
        region_data = {}
        region_data['name'] = region['properties']['sa2_name11']
        region_data['geometry'] = region['geometry']
        db[region['properties']['sa2_main11']] = region_data
