# imports all regional stats from a csv file into a couchdb database
import sys
import json
import couchdb
import pandas
from my_couchdb import set_up_db

USERNAME = 'azhou'
PASSWORD = 'potato'
GEO_DB_NAME = 'geo'
DB_NAME = 'income'
FILE = '/home/anya/Documents/Uni/Y2S1/CCC/Assignment2/abs/regional_income.csv'
CODE_COL_NAME = 'SA2'

# # Get the database details
# USERNAME = input('Enter the admin name: ')
# PASSWORD = input('Enter the admin password: ')
# DB_NAME = input('Enter the name of the geo-area database: ')
# DB_NAME = input('Enter the name of the new database: ')
# FILE = input('Enter the path to the import file: ')
# CODE_COL_NAME = input('Enter the name of the column with the region code: ')

#  Grab the couchdb server url from the command line (if any)
if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
geo_db = server[GEO_DB_NAME]
db = set_up_db(DB_NAME, server)

region_df = pandas.read_csv(FILE)
region_json_str = region_df.to_json(orient='records')
region_json = json.loads(region_json_str)

for region in region_json:
    print(region)
    if str(region[CODE_COL_NAME]) in geo_db:
        doc = {'_id': str(region[CODE_COL_NAME])}
        del region[CODE_COL_NAME]
        doc.update(region)
        db.save(doc)
        print(doc)
