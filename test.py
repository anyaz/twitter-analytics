import sys
import json, geojson
from shapely.geometry import shape, Point, Polygon, MultiPolygon
import couchdb
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

USERNAME = 'azhou'
PASSWORD = 'potato'
GEO_DB_NAME = 'geo'
TWT_DB_NAME = 'test'

if len(sys.argv) > 1:
    server_url = sys.argv[1]
    server = couchdb.Server(server_url)
else:
    # Use localhost as default
    server_url = 'localhost:5984'
    server = couchdb.Server()

server.resource.credentials = (USERNAME, PASSWORD)
twt_db = server[TWT_DB_NAME]
geo_db = server[GEO_DB_NAME]

if '206021111' in geo_db:
    print('good')
else:
    print('nope')
